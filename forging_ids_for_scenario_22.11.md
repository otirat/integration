# Forging IDs in did:web for scenario 22.11

## 1. Purpose of this document

The purpose of this document is to propose a format for did:web identifiers for the json-ld objects used in the scenario 22.11. The proposed format will be detailed and discussions about components and functionalities would be exposed

## 2. Proposed format for ids

### 2.1 Preliminary Assumptions

The proposed id format assumes that all valid objects that are described in the gaia-x scenario 22.11 data infrastructure are delivered by a participant (participant to a federator). To validate its objects this participant needs cryptographic materials registered the appropriate way in the trusted registry. 

This allow the participant to forge Verifiable Credential that can be used in the GAIA-X Trust Framework to assess object properties as claimed.

We will assumes that claimed object properties are valid if there is a valid VC, issued by the one who hold the identifier of the object, that claims the value(s) of this object properties. 

We will now build our did:web identifiers by starting by the participant id

### 2.2 Participant Id

As preliminary assumptions we have assumed that every objects in the data infrastucture are identified by a federation participant. The federation maintains a trust registry to be able to verify the validity of the assumption made on the participant object. 

The first object and the first assumption to be made is the assumption about the participant id. This participant id is a unique id that identify the participant worldwide with no other concern that it must identify the participant itself. 

To forge this id we will forge a hash on identifiyinf mandatory data for the participant. 
For the scenario we have proposed to use:

```
# Participants
id = sha256(participant.name+participant + "+" + participant.registration_number + '+' + participant.hqa_country)
```
### 2.3 Proposed Participand id in did:web format

Once forged the participant id we have to assume that the participant have to expose an access  endpoint to use the service exposed by the participant. One of the service required by the participant is to make available the public key used to verifign the VC the participant as issued. 

In did:web format this is done by providing a well composed did.json files for the route .well-known/did.json

The proposed did:web id for the participant would be the following

```
did:web:<fqdn_of_access_endpoint_for_the_participant>:participant:<pariticipant id>
```
That way the participant object would be resolved in:
```
   https://<fqdn_of_access_endpoint_for_the_participant>/participant/<pariticipant id>/data.json
```
and the public key and all associated material:
```
   https://<fqdn_of_access_endpoint_for_the_participant>/participant/<pariticipant id>/.well-known/did.json
```
### 2.4 Other objects identified by the participant

Any other objects identified by the participant would be identified according to the following rules and stored according to the scheme of identification.

```
   did:web:<fqdn_of_access_endpoint_for_the_participant>/participant/<pariticipant id>/<object_type>/<object_id>
```
object_type is the lowest level object type of the object, which means the deepest in the inheritance chain. The string format is the same used in the yaml format to describe the ontology.

the object_id is as following:
```
id = sha256(participant_object_id)
```
where participant_object_id is a string which is unique by design for this object type. This is the duty of the participant to ensure the uniqueness of participant_id. 
In easy to maintain implementation all GAIA-X object includes a properties named hasParticipantObjectId (xsd:string) that maybe used to reference this object and search over this type of objects for the participant. 
This property may be used by the participant to reference objects in his own identification domain. 

The hasParticipantObjectId property can be used as the only content of participant_object_id if the participant guarantees the uniqueness of this property for the same object type. 

## 3. Discussions

### 3.1 Pros and Cons

### 3.2 Role of the participant Agent in forging Ids

As a broadcast software component, the participant agent has a central role in giving access to the data identified by the participant. In "Scenario 22.11", it forges most of the object ids. 
It seems to be relevant to have only one component forging the id of object for each participant. It will ease and centralized the change. The participant Agent seems to be the right one for Scenario 22.11


### 3.3 Proxying or CDN behaviour through id

The benefit of this format will be the easiness of giving access to any object identified by any other participant. 
If a participant2 agent stores the object that has been identified by participant agent 1 the way same he stores the object he has identified, participant agent2 can give access to participant 1 object just by substituing participant1 fqdn

That way by broadcasting the federationg participant identified objects with such idenfitier and storing format, the federation can give access to the whole set of objects in any user agent. This is a first step to a fully distributed infrastructure for the federation. 






